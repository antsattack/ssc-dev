﻿using System;
using Xamarin.Forms;

namespace ShopNaSuaCasa.Controls
{
    public class CustomMenuCell : ViewCell
    {
        public CustomMenuCell()
        {

            Image menuicon = new Image();
            menuicon.SetBinding(Image.SourceProperty, "Image");

            Label lblTitle = new Label()
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                FontSize = 17f,
                FontAttributes = FontAttributes.Bold,
                TextColor = Color.FromHex("#2d3b93")
            };
            lblTitle.SetBinding(Label.TextProperty, "Title");

            var layout = new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                Spacing = 20,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Children = { menuicon, lblTitle },
                Padding = new Thickness(20, 10, 0, 0)
            };
            this.View = layout;
        }
    }
}
