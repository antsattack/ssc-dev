﻿using System;
using ShopNaSuaCasa.Interfaces;
using Refit;

namespace ShopNaSuaCasa.Services
{
    public class NetworkService : INetworkService
    {
        public IAntsAttackApiService apiService;

        string BaseUrl = "https://api.antsattack.com/v1";

        public IAntsAttackApiService GetApiService()
        {
            apiService = RestService.For<IAntsAttackApiService>(BaseUrl);
            return apiService;
        }


    }
}
 