﻿using System;
using System.Threading.Tasks;
using Refit;
using ShopNaSuaCasa.Models;
using System.Net.Http;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ShopNaSuaCasa.Interfaces
{
    public interface IAntsAttackApiService
    {
        [Post("/auth")]
        Task<AuthResponse> Auth();

        [Get("/advisors/{id}")]
        Task<AdvisorResponse> ListAdvisor([Header("X-Token")] string token, int id);

        [Post("/products")]
        Task<int> AddProduct([Header("X-Token")] string token,
                                            [Body] NewProductRequest newProductRequest);
        /*
        [Multipart]
        [Post("/images")]
        Task<HttpResponseMessage> AddImages([Header("X-Token")] string token,
                                             AddImagesRequest addImagesRequest ); */


        [Post("/images")]
        Task<HttpResponseMessage> AddImages([Header("X-Token")] string token,
                                             AddImagesRequest addImagesRequest);

        [Get("/categories")]
        Task<ObservableCollection<Category>> ListAllCategories([Header("X-Token")] string token);

        [Get("/categories/{id}")]
        Task<IList<Category>> ListCategoryById([Header("X-Token")] string token,
                                               int id);

        [Get("/tags/category/{idcategory}")]
        Task<ObservableCollection<Tag>> ListTagsByCategory([Header("X-Token")] string token,
                                                           int idcategory);

        [Get("/datasheets/category/{idcategory}")]
        Task<ObservableCollection<Datasheets>> ListDatasheetsByCategory([Header("X-Token")] string token,
                                                           int idcategory);

        [Get("/categories/{idproduct}")]
        Task<int> PublishProduct([Header("X-Token")] string token,
                                           [Body] int visible, int idproduct);

        [Get("/images/product/{idproduct}")]
        Task<ObservableCollection<ProductImage>> ListProductImages([Header("X-Token")] string token,
                                                             int idproduct);
        [Delete("/images/{idimage}")]
        Task<string> DeleteImageById([Header("X-Token")] string token,
                                                             int idimage);

        [Patch("/images/main/product/{idimage}")]
        Task<string> SetMainImage([Header("X-Token")] string token,
                                 int idimage);


    }
}