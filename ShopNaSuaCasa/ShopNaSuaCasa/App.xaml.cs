﻿using Prism;
using Prism.Ioc;
using ShopNaSuaCasa.ViewModels;
using ShopNaSuaCasa.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Prism.DryIoc;
using DLToolkit.Forms.Controls;
using ShopNaSuaCasa.Interfaces;
using ShopNaSuaCasa.Services;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace ShopNaSuaCasa
{
    public partial class App : PrismApplication
    {
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            InitializeComponent();
            FlowListView.Init();
             

            //await NavigationService.NavigateAsync(nameof(NewProductPage));
            await NavigationService.NavigateAsync(nameof(MainPage) + "/" + nameof(NavigationPage) + "/" + nameof(ProductsPage));
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<LeftMenuPage>();
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<MainPage>();
            containerRegistry.RegisterForNavigation<MyPage>();
            containerRegistry.RegisterForNavigation<ProductsPage>();
            containerRegistry.RegisterForNavigation<LoginPage>();

            // New product flow
            containerRegistry.RegisterForNavigation<NewProductPage>();
            containerRegistry.RegisterForNavigation<AddPhotosPage>();
            containerRegistry.RegisterForNavigation<ChooseCategoriesPage>();
            containerRegistry.RegisterForNavigation<ChooseTagsPage>();
            containerRegistry.RegisterForNavigation<AddProductDetailsPage>(); 
            containerRegistry.RegisterForNavigation<PublishProductPage>();

            containerRegistry.Register<INetworkService, NetworkService>();

        }
    }
}
