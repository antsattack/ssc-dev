﻿using System;
using System.Threading.Tasks;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using ShopNaSuaCasa.Interfaces;
using Xamarin.Forms;

namespace ShopNaSuaCasa.ViewModels
{
    public class PublishProductPageViewModel : ViewModelBase
    {
        string token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyaWQiOjEsInVzZXJuYW1lIjoiWlx1MDBlOSBNYXJpYSBNYWdhemluIn0.gIyauhy6WmMM8E5iF77rr_RGuhHNWz7bbEgdZckVQDAlEw1dPw_IwPuiebwB9cvVeCsJrYOSE_SL9eaunKHp3g";


        private INavigationService _navigationService;
        private IPageDialogService _pageDialogService;
        private INetworkService _networkService;
        public int _productId;

        public DelegateCommand PreviousPageCommand { get; set; }
        public DelegateCommand NextPageCommand { get; set; }
        public DelegateCommand SwitchToggledCommand { get; set; }

        public bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { SetProperty(ref _isSelected, value); }
        }

        //PRODUTO VISÍVEL AO PÚBLICO

        private string _publishProductTitle = "PRODUTO INVISÍVEL";
        public string PublishProductTitle
        {
            get { return _publishProductTitle; }
            set { SetProperty(ref _publishProductTitle, value); }
        }


        bool _isRunning;
        public bool IsRunning
        {
            get { return _isRunning; }
            set { SetProperty(ref _isRunning, value); }
        }

        string _stepText;
        public string StepText
        {
            get { return _stepText; }
            set { SetProperty(ref _stepText, value); }
        }

        public PublishProductPageViewModel(INavigationService navigationService,
                                             IPageDialogService dialogService,
                                             INetworkService networkService)
            :base (navigationService)
        {
            Title = "Cadastro de Produto";

            _navigationService = navigationService;
            _pageDialogService = dialogService;
            _networkService = networkService;

            PreviousPageCommand = new DelegateCommand(PreviousPage);
            //NextPageCommand = new DelegateCommand(NextPage);
            //SwitchToggledCommand = new DelegateCommand(OnSwitchToggled);

            _isSelected = false;

   

        }

        void PreviousPage()
        {
            _navigationService.GoBackAsync();
        }

        public override async void OnNavigatedTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey("ProductId"))
            {
                _productId = (int)parameters["ProductId"];
            }

            await ListAdvisors();

            //Categories = new ObservableCollection<Category>(await ListMainCategory());

        }

        void OnSwitchToggled(object sender, ToggledEventArgs e)
        {
            //label.Text = String.Format("Switch is now {0}", e.Value);
            Console.WriteLine(e.Value);
        }

        async Task<int> PublishProduct(int visible)
        {


            try
            {
                return await _networkService.GetApiService()
                                            .PublishProduct(token,2,25);


            }
            catch (Exception ex)
            {
                //log
                Console.WriteLine(ex);

            }
            finally
            {
                //hide placeholder message
                if(visible==1){
                    PublishProductTitle = "PRODUTO VISÍVEL AO PÚBLICO";
                }else{
                    PublishProductTitle = "PRODUTO INVISÍVEL AO PÚBLICO";
                }
               

            }

            return visible;
        }

        async Task ListAdvisors()
        {
            try
            {
                var response = await _networkService.GetApiService()
                                                      .ListAdvisor(token, 1);
                //return response.step1;
                StepText = response.step6;
                IsRunning = false;
            }
            catch
            {
                
            }finally
            {
                IsRunning = false;
            }
        } 
    }
}
