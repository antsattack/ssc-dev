﻿using System;
using Prism.Navigation;
using Prism.Commands;
using System.Collections.ObjectModel;
using ShopNaSuaCasa.Models;
using Prism.Services;
using ShopNaSuaCasa.Interfaces;
using System.Collections.Generic;
using Prism.Mvvm;
using System.Threading.Tasks;

namespace ShopNaSuaCasa.ViewModels
{
    public class AddProductDetailsPageViewModel : ViewModelBase
    {
        string token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyaWQiOjEsInVzZXJuYW1lIjoiWlx1MDBlOSBNYXJpYSBNYWdhemluIn0.gIyauhy6WmMM8E5iF77rr_RGuhHNWz7bbEgdZckVQDAlEw1dPw_IwPuiebwB9cvVeCsJrYOSE_SL9eaunKHp3g";

        private INavigationService _navigationService;
        private IPageDialogService _pageDialogService;
        private INetworkService _networkService;

        public DelegateCommand PreviousPageCommand { get; set; }
        public DelegateCommand NextPageCommand { get; set; }

        public ObservableCollection<Datasheets> _datasheets;

        public ObservableCollection<Datasheets> ListDatasheet
        {
            get { return _datasheets; }
            set { SetProperty(ref _datasheets, value); }
        }


        bool _isRunning;
        public bool IsRunning
        {
            get { return _isRunning; }
            set { SetProperty(ref _isRunning, value); }
        }

        string _stepText;
        public string StepText
        {
            get { return _stepText; }
            set { SetProperty(ref _stepText, value); }
        }

        public int _productId;

        public string Teste = "testeeee";

        public AddProductDetailsPageViewModel(INavigationService navigationService,
                                             IPageDialogService dialogService,
                                             INetworkService networkService)
        : base(navigationService)
        {
            Title = "Cadastro de Produto";

            _navigationService = navigationService;
            _pageDialogService = dialogService;
            _networkService = networkService;

            PreviousPageCommand = new DelegateCommand(PreviousPage);
            NextPageCommand = new DelegateCommand(NextPage);


            // iniciar lista
            _datasheets = new ObservableCollection<Datasheets>();
            _datasheets.Add(new Datasheets(2, "Cor predominante",
                                           new List<Object> { new Color(1,"Azul","#ffffff"),
                                           new Color(1,"Verde","#ffffff") }));
            _datasheets.Add(new Datasheets(2, "Material sintético", new List<Object>(new Object[] { "sim", "não" })));
            _datasheets.Add(new Datasheets(2, "Material sintético", new List<Object>(new Object[] { "P", "M", "G" })));

        }

        void PreviousPage()
        {
            _navigationService.GoBackAsync();
        }

        async void NextPage()
        {
            var navParameters = new NavigationParameters();
            navParameters.Add("ProductId", _productId);

            await _navigationService.NavigateAsync(new Uri("PublishProductPage", UriKind.Relative), navParameters);
        } 
        

        public override async void OnNavigatedTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey("ProductId"))
            {
                _productId = (int) parameters["ProductId"];
            }

            await ListAdvisors();

            ListDatasheet = new ObservableCollection<Datasheets>(await ListDatasheetsByCategory());

            Console.WriteLine("List Datasheet");
           
        }


        async Task<ObservableCollection<Datasheets>> ListDatasheetsByCategory()
        {


            try
            {
                return await _networkService.GetApiService()
                  .ListDatasheetsByCategory(token, 25);
                

            }
            catch (Exception ex)
            {
                //log
                Console.WriteLine(ex);

            }
            finally
            {
                //hide placeholder message
                //IsPickerEnabled = true;
                //PickerTitle = "Selecione uma categoria";
           

            }

            return null;
        }

        async Task ListAdvisors()
        {
            try
            {
                var response = await _networkService.GetApiService()
                                                      .ListAdvisor(token, 1);
                //return response.step1;
                StepText = response.step5;
                IsRunning = false;
            }
            catch
            {
                
            }finally
            {
                IsRunning = false;
            }
        } 

    

    }

}
