﻿using System;
using Prism.Navigation;
using System.Collections.ObjectModel;
using Prism.Commands;
using Prism.Services;
using Plugin.Media;
using ShopNaSuaCasa.Models;
using ShopNaSuaCasa.Interfaces;
using Xamarin.Forms;
using System.IO;
using System.Threading.Tasks;
using Refit;
using System.Collections.Generic;
using System.Collections;
using System.Net.Http;
using System.Text;
using System.Linq;

namespace ShopNaSuaCasa.ViewModels
{
    public class AddPhotosPageViewModel : ViewModelBase
    {

        private string token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyaWQiOjEsInVzZXJuYW1lIjoiWlx1MDBlOSBNYXJpYSBNYWdhemluIn0.gIyauhy6WmMM8E5iF77rr_RGuhHNWz7bbEgdZckVQDAlEw1dPw_IwPuiebwB9cvVeCsJrYOSE_SL9eaunKHp3g";

        private INavigationService _navigationService;
        private IPageDialogService _pageDialogService;
        private INetworkService _networkService;

        public ObservableCollection<ProductImage> _items;
       
        public ObservableCollection<ProductImage> Items
        {
            get { return _items; }
            set { SetProperty(ref _items, value);}
        } 

        public DelegateCommand PreviousPageCommand { get; set; }
        public DelegateCommand NextPageCommand { get; set; }
        public DelegateCommand AddPhotoCommand { get; set; }
        public DelegateCommand FlagMainImageCommand { get; set; }
        public DelegateCommand<ProductImage> DeleteImageCommand { get; set; }

        public int _productId;

        bool _isRunning;
        public bool IsRunning
        {
            get { return _isRunning; }
            set { SetProperty(ref _isRunning, value); }
        }

        string _stepText;
        public string StepText
        {
            get { return _stepText; }
            set { SetProperty(ref _stepText, value); }
        }



        public AddPhotosPageViewModel(INavigationService navigationService,
                                      IPageDialogService dialogService,
                                      INetworkService networkService)
            : base(navigationService)
        {
            Title = "Cadastro de Produto";
            IsRunning = true;
            _navigationService = navigationService;
            _pageDialogService = dialogService;
            _networkService = networkService;

            PreviousPageCommand = new DelegateCommand(PreviousPage);
            NextPageCommand = new DelegateCommand(NextPage);
            AddPhotoCommand = new DelegateCommand(displayImageChooser);
            DeleteImageCommand = new DelegateCommand<ProductImage>(DeleteImage);
            //_items = new ObservableCollection<ProductImage>();


           // Items = _items;

        }

        public override async void OnNavigatedTo(NavigationParameters parameters)
        {
            //You can check if parameters has value before passing it inside

            if (parameters.ContainsKey("ProductId"))
            {
                _productId = (int)parameters["ProductId"];
            }

            Items = new ObservableCollection<ProductImage>(await ListProductImages());

            await ListAdvisors();
        }

        private void PreviousPage()
        {
            _navigationService.GoBackAsync();
        }


        async void PickImageFromGallery()
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                await _pageDialogService.DisplayAlertAsync("Photos Not Supported", ":( Permission not granted to photos.", "OK");
                return;
            }
            try
            {
                Stream stream = null;
                var file = await CrossMedia.Current.PickPhotoAsync().ConfigureAwait(true);


                if (file == null)
                    return;

                stream = file.GetStream();
                file.Dispose();

                //image.Source = ImageSource.FromStream(() => stream);

            }
            catch (Exception ex)
            {
                // Xamarin.Insights.Report(ex);
                // await DisplayAlert("Uh oh", "Something went wrong, but don't worry we captured it in Xamarin Insights! Thanks.", "OK");
            }


        }

        async void displayImageChooser()
        {
            var action = await _pageDialogService.DisplayActionSheetAsync("Adicionar imagem", "Câmera", "Galeria", "Selecione uma opção");
            //await _pageDialogService.DisplayAlertAsync("Teste", "" + action, "OK");

            if(action.Equals("Galeria"))
            {
                PickImageFromGallery();
            }

            takePhoto();

        }

        async void takePhoto()
        {

            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await _pageDialogService.DisplayAlertAsync("Erro", "Não há câmera disponível.", "ENTENDI");
                return;
            }
            try
            {
                var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
                {

                    //SaveToAlbum = saveToGallery.IsToggled
                    SaveToAlbum = true
                });

                if (file == null)
                    return;

                //await _pageDialogService.DisplayAlertAsync("File Location", (true ? file.AlbumPath : file.Path), "OK");

                //Image image = new Image();
                /*
                image.Source = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    file.Dispose();
                    return stream;
                }); */

                _items.Add(new ProductImage(){id=1,url=file.Path});
            }
            catch (Exception ex)
            {
                // Xamarin.Insights.Report(ex);
               // await _pageDialogService.DisplayAlertAsync("Adicionar Imagem", "Ocorreu um erro ao capturar imagem da câmera", "Entendi");
            }
        }

        async void NextPage()
        {
           
            var navParameters = new NavigationParameters();
            navParameters.Add("ProductId", _productId);

            await _navigationService.NavigateAsync(new Uri("ChooseCategoriesPage", UriKind.Relative), navParameters);
        } 

        async Task ListAdvisors()
        {
            try
            {
                
                var response = await _networkService.GetApiService()
                                                      .ListAdvisor(token, 1);
                //return response.step1;
                StepText = response.step2;
                IsRunning = false;
            }catch
            {
                
            }finally
            {
                IsRunning = false;
            }
        } 

        async Task<ObservableCollection<ProductImage>> ListProductImages()
        {


            try
            {
                return await _networkService.GetApiService()
                                            .ListProductImages(token,_productId);

            }
            catch (Exception ex)
            {
                //log
                Console.WriteLine(ex);

            }
            finally
            {
                //hide placeholder message

            }

            return null;
        } 


      

        async void DeleteImage(ProductImage productImage)
        {
            Console.WriteLine("teste imagem delete: " + productImage.id);
            await DeleteImageById(productImage.id);

        }

        async Task<string> DeleteImageById(int idImage)
        {


            try
            {
                return await _networkService.GetApiService()
                                            .DeleteImageById(token,idImage);

            }
            catch (Exception ex)
            {
                //log
                Console.WriteLine(ex);

            }
            finally
            {
                //hide placeholder message
                foreach (var item in Items.Where(x => x.id == idImage).ToList())
                {
                    Items.Remove(item);
                }
            }

            return null;
        } 


    }


}
