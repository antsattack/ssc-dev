using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Prism.Commands;
using Prism.Navigation;
using Prism.Logging;
using Prism.Services;
using ShopNaSuaCasa.Interfaces;
using ShopNaSuaCasa.Models;
using Xamarin.Forms;
using ShopNaSuaCasa.Views;
using System.Threading;

namespace ShopNaSuaCasa.ViewModels
{
    public class NewProductPageViewModel : ViewModelBase
    {
        private string token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyaWQiOjEsInVzZXJuYW1lIjoiWlx1MDBlOSBNYXJpYSBNYWdhemluIn0.gIyauhy6WmMM8E5iF77rr_RGuhHNWz7bbEgdZckVQDAlEw1dPw_IwPuiebwB9cvVeCsJrYOSE_SL9eaunKHp3g";
        private INavigationService _navigationService;
        public DelegateCommand NextPageCommand { get; set; }
        public DelegateCommand AddProductCommand { get; set; }
        public DelegateCommand OnValidationCommand { get; set; }
    

        string _stepText;
        public string StepText
        {
            get { return _stepText; }
            set { SetProperty(ref _stepText, value); }
        }

        bool _isRunning;
        public bool IsRunning
        {
            get { return _isRunning; }
            set { SetProperty(ref _isRunning, value); }
        }

        string _productTitleErrorText;
        public string ProductTitleErrorText
        {
            get { return _productTitleErrorText; }
            set { SetProperty(ref _productTitleErrorText, value); }
        }

        string _productDescriptionErrorText;
        public string ProductDescriptionErrorText
        {
            get { return _productDescriptionErrorText; }
            set { SetProperty(ref _productDescriptionErrorText, value); }
        }


        string _productTitle;
        public string ProductTitle
        {
            get { return _productTitle; }
            set { SetProperty(ref _productTitle, value); }
        }

        string _productDescription;
        public string ProductDescription
        {
            get { return _productDescription; }
            set { SetProperty(ref _productDescription, value); }
        }

        bool _isBackButtonEnabled;
        public bool IsBackButtonEnabled
        {
            get { return _isBackButtonEnabled; }
            set { SetProperty(ref _isBackButtonEnabled, value); }
        }

        private INetworkService _networkService;
        private IPageDialogService _pageDialogService;
        public User User { get; set; }

        public NewProductPageViewModel(INavigationService navigationService,
                                       INetworkService networkService,
                                      IPageDialogService pageDialogService)
            : base(navigationService)
        {
            IsRunning = true;
            _navigationService = navigationService;
            _networkService = networkService;
            _pageDialogService = pageDialogService;

            Console.WriteLine("navigation object:" + _navigationService);
            Title = "Cadastro de Produto";

            /*
            OnValidationCommand = new DelegateCommand(() =>
            {
                User.Email.NotValidMessageError = "Título é obrigatório";
                User.Email.IsNotValid = string.IsNullOrEmpty(User.Email.Name);

                //User.Email.NotValidMessageError = "Email is required";
                //User.Email.IsNotValid = string.IsNullOrEmpty(User.Email.Name);

                /*
                if (string.IsNullOrEmpty(User.Password.Name))
                {
                    User.Password.NotValidMessageError = "Password is required";
                    User.Password.IsNotValid = true;
                }
                else if (User.Password.Name.Length < 5)
                {
                    User.Password.NotValidMessageError = "Password must have more than 5 charcteres";
                    User.Password.IsNotValid = true;
                }
                else
                {
                    User.Password.IsNotValid = false;
                } 

            }); */

            NextPageCommand = new DelegateCommand(NextPage);


           
        }


        public override async void OnNavigatedTo(NavigationParameters parameters)
        {

            //Step = new StepModel(await ListAdvisors(), false, true);
            //StepText = await ListAdvisors();
            IsBackButtonEnabled = false;
            await ListAdvisors();
        }

 
        async void NextPage()
        {

            var IsValid = false;

            if(string.IsNullOrEmpty(ProductTitle))
            {
                ProductTitleErrorText = "Título é obrigatório";
                IsValid = false;
            }else{
                ProductTitleErrorText = null;
                IsValid = true;
            }
            /*
            if (string.IsNullOrEmpty(ProductDescription))
            {
                IsValid = false;
                ProductDescriptionErrorText = "Campo obrigatório";
            }
            else
            {
                ProductDescriptionErrorText = null;
                IsValid = true;
            } */

            if(IsValid)
            {
                var navParameters = new NavigationParameters();
                //navParameters.Add("ProductId", await AddProduct(new NewProductRequest(Title, ProductDescription)));
                navParameters.Add("ProductId", await AddProductMock(new NewProductRequest(Title, ProductDescription)));

                await _navigationService.NavigateAsync(new Uri("AddPhotosPage", UriKind.Relative), navParameters);
            }
            /*
            if(String.IsNullOrEmpty(Title) && !String.IsNullOrEmpty(ProductDescription))
            {
                
                var navParameters = new NavigationParameters();

                navParameters.Add("ProductId", await AddProduct(new NewProductRequest(Title, ProductDescription)));


                await _navigationService.NavigateAsync(new Uri("AddPhotosPage", UriKind.Relative), navParameters);
            }
            else
            {
                await _pageDialogService.DisplayAlertAsync("Dados do Produto", "Por favor, preencha todos os campos obrigatórios", "ENTENDI");
            }
            */
        }

    

        async Task<int> AddProduct(NewProductRequest newProductRequest)
        {
            
            return await _networkService
                            .GetApiService()
                            .AddProduct(token,
                             newProductRequest);
            
        }

        async Task<int> AddProductMock(NewProductRequest newProductRequest)
        {

            Thread.Sleep(500);
            return 41;

        }

        async Task ListAdvisors()
        {
            try
            {
                var response = await _networkService.GetApiService()
                                                      .ListAdvisor(token, 1);
                //return response.step1;
                StepText = response.step1;
                IsRunning = false;
            }catch
            {
                
            }finally
            {
                IsRunning = false;
            }
        } 

    }
}
