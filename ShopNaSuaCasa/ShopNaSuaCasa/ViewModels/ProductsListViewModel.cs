﻿using System;
using Prism.Services;
using Prism.Navigation;
using ShopNaSuaCasa.Models;

namespace ShopNaSuaCasa.ViewModels
{
    public class ProductsListViewModel : ViewModelBase
    {
        INavigationService _navigationService;

        public ProductsListViewModel(INavigationService navigationService):base(navigationService)
        {
            
        }
        /*
        #region Properties.
        public IProductService ProductService { get; set; }

        public IDataStore<Product> DataStoreService { get; set; }

        public int Category_id { get; set; }

        public Models.LoginResponse User { get { return App.Locator.Login.UserLoggedIn; } }

        public ObservableCollection<Models.Product> ProductsList { get; set; }

        public bool IsVisibleProductsList { get; set; } = false;
        #endregion

        public ProductsListViewModel(IProductService productService, IProgressBar progressBar, IDataStore<Product> dataStoreService)
        {
            this.ProductService = productService;
            this.ProgressBar = progressBar;
            this.Category_id = 0;
            this.ProductsList = new ObservableCollection<Models.Product>();
            this.DataStoreService = dataStoreService;
        }

        #region Get Products By Category.
        private RelayCommand getProductsByCatCommand;
        public RelayCommand GetProductsByCatCommand
        {
            // get from api
            // get { return getProductsByCatCommand ?? (getProductsByCatCommand = new RelayCommand(async () => await ExecuteGetProductsByCatCommand())); }

            // get from mock data
            get { return getProductsByCatCommand ?? (getProductsByCatCommand = new RelayCommand(async () => await ExecuteGetMockDataCommand())); }
        }

        async Task ExecuteGetProductsByCatCommand()
        {
            try
            {
                if (IsNetworkAvailable)
                {
                    this.ProgressBar.ShowProgress();

                    this.IsVisibleProductsList = false;
                    RaisePropertyChanged("IsVisibleProductsList");

                    if (this.ProductsList != null)
                        this.ProductsList.Clear();

                    var response = await this.ProductService.ProductsByCategoryAsync(this.Category_id);
                    if (response.Msg.Equals(this.Success))
                    {
                        this.ProductsList = new ObservableCollection<Models.Product>(response.Products as List<Models.Product>);
                        this.IsVisibleProductsList = true;
                        RaisePropertyChanged("IsVisibleProductsList");
                        RaisePropertyChanged("ProductsList");
                    }
                }
                else
                    await this.CurrentContentPage.DisplayAlert("", this.InternetError, "OK");
            }
            catch (Exception ex)
            {
                await this.CurrentContentPage.DisplayAlert("", ex.Message, "OK");
            }
            finally
            {
                this.ProgressBar.Dismiss();
            }
        }
        #endregion

        #region Get Products From DataStore
        async Task ExecuteGetMockDataCommand()
        {
            try
            {
                this.ProgressBar.ShowProgress();

                this.IsVisibleProductsList = false;
                RaisePropertyChanged("IsVisibleProductsList");

                if (this.ProductsList != null)
                    this.ProductsList.Clear();

                var response = await this.DataStoreService.GetProductsAsync(false);
                if (!response.Equals(null))
                {
                    this.ProductsList = new ObservableCollection<Models.Product>(response as List<Models.Product>);
                    this.IsVisibleProductsList = true;
                    RaisePropertyChanged("IsVisibleProductsList");
                    RaisePropertyChanged("ProductsList");
                }
                // this.ProgressBar.Dismiss();
            }
            catch (Exception ex)
            {
                await this.CurrentContentPage.DisplayAlert("", ex.Message, "OK");
            }
            finally
            {
                await Task.Delay(2500);
                this.ProgressBar.Dismiss();
            }
        }
        #endregion
    
    }
    */

    }
}
