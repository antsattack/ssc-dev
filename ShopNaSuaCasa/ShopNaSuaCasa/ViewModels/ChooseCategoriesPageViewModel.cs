﻿using System;
using Prism.Navigation;
using Prism.Commands;
using Prism.Services;
using ShopNaSuaCasa.Interfaces;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using ShopNaSuaCasa.Models;
using Xamarin.Forms;

namespace ShopNaSuaCasa.ViewModels
{
    public class ChooseCategoriesPageViewModel : ViewModelBase
    {
        string token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyaWQiOjEsInVzZXJuYW1lIjoiWlx1MDBlOSBNYXJpYSBNYWdhemluIn0.gIyauhy6WmMM8E5iF77rr_RGuhHNWz7bbEgdZckVQDAlEw1dPw_IwPuiebwB9cvVeCsJrYOSE_SL9eaunKHp3g";

        private INavigationService _navigationService;
        private IPageDialogService _pageDialogService;
        private INetworkService _networkService;

        public DelegateCommand PreviousPageCommand { get; set; }
        public DelegateCommand NextPageCommand { get; set; }
        public int _productId;

        public ObservableCollection<Category> _categories;
       
        public ObservableCollection<Category> Categories
        {
            get { return _categories; }
            set { SetProperty(ref _categories, value);}
        } 

        bool _isRunning;
        public bool IsRunning
        {
            get { return _isRunning; }
            set { SetProperty(ref _isRunning, value); }
        }

        string _stepText;
        public string StepText
        {
            get { return _stepText; }
            set { SetProperty(ref _stepText, value); }
        }

        public Boolean IsPickerEnabled;

        public string _pickerTitle;
       
        public string PickerTitle
        {
            get { return _pickerTitle; }
            set { SetProperty(ref _pickerTitle, value); }
        }

        public ChooseCategoriesPageViewModel(INavigationService navigationService,
                                             IPageDialogService dialogService,
                                             INetworkService networkService)
            : base(navigationService)
        {
            Title = "Cadastro de Produto";
            PickerTitle = "Buscando categorias...";

            _navigationService = navigationService;
            _pageDialogService = dialogService;
            _networkService = networkService;

            PreviousPageCommand = new DelegateCommand(PreviousPage);
            NextPageCommand = new DelegateCommand(NextPage);
   

        }

        public override async void OnNavigatedTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey("ProductId"))
            {
                _productId = (int)parameters["ProductId"];
            }
            await ListAdvisors();

            Categories = new ObservableCollection<Category>(await ListMainCategory());

        }

        public async void OnPickerSelectedIndexChanged(EventArgs sender)
        {
            await _pageDialogService.DisplayAlertAsync("Picker", "Evento " + sender.ToString(), "OK");
        }

        void PreviousPage()
        {
            _navigationService.GoBackAsync();
        }

        async void NextPage()
        {
            var navParameters = new NavigationParameters();
            navParameters.Add("ProductId", _productId);

            await _navigationService.NavigateAsync(new Uri("ChooseTagsPage", UriKind.Relative), navParameters);
        } 


        async Task<ObservableCollection<Category>> ListMainCategory()
        {
          

            try
            {
                return await _networkService.GetApiService()
                  .ListAllCategories(token);


            }
            catch (Exception ex)
            {
                //log
                Console.WriteLine(ex);

            }
            finally
            {
                //hide placeholder message
                IsPickerEnabled = true;
                PickerTitle = "Selecione uma categoria";

            }

            return null;
        }

        async Task ListAdvisors()
        {
            try
            {
                var response = await _networkService.GetApiService()
                                                      .ListAdvisor(token, 1);
                //return response.step1;
                StepText = response.step3;
                IsRunning = false;
            }catch
            {
                
            }finally
            {
                IsRunning = false;
            }
        } 

       


    }


}
