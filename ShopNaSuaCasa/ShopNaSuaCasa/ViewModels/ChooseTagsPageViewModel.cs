﻿using System;
using Prism.Navigation;
using System.Collections.ObjectModel;
using Prism.Commands;
using System.Threading.Tasks;
using ShopNaSuaCasa.Models;
using ShopNaSuaCasa.Interfaces;
using Prism.Services;

namespace ShopNaSuaCasa.ViewModels
{
    public class ChooseTagsPageViewModel: ViewModelBase
    {

        string token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyaWQiOjEsInVzZXJuYW1lIjoiWlx1MDBlOSBNYXJpYSBNYWdhemluIn0.gIyauhy6WmMM8E5iF77rr_RGuhHNWz7bbEgdZckVQDAlEw1dPw_IwPuiebwB9cvVeCsJrYOSE_SL9eaunKHp3g";

        private INavigationService _navigationService;
        private IPageDialogService _pageDialogService;
        private INetworkService _networkService;

        public ObservableCollection<Tag> _items;

        public ObservableCollection<Tag> Items
        {
            get { return _items; }
            set { SetProperty(ref _items, value); }
        }


        bool _isRunning;
        public bool IsRunning
        {
            get { return _isRunning; }
            set { SetProperty(ref _isRunning, value); }
        }

        string _stepText;
        public string StepText
        {
            get { return _stepText; }
            set { SetProperty(ref _stepText, value); }
        }
       

        public int _productId;

        public DelegateCommand PreviousPageCommand { get; set; }
        public DelegateCommand NextPageCommand { get; set; }
     
        public ChooseTagsPageViewModel(INavigationService navigationService,
                                       IPageDialogService dialogService,
                                       INetworkService networkService)
            : base(navigationService)
        {
            Title = "Cadastro de Produto";
            _navigationService = navigationService;
            _pageDialogService = dialogService;
            _networkService = networkService;

            PreviousPageCommand = new DelegateCommand(PreviousPage);
            NextPageCommand = new DelegateCommand(NextPage);

                 
        }

        public override async void OnNavigatedTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey("ProductId"))
            {
                _productId = (int)parameters["ProductId"];
            }

            await ListAdvisors();

            Items = new ObservableCollection<Tag>(await ListTagsByCategory());
            Console.WriteLine(Items);
        }

        private void PreviousPage()
        {
            //TODO: call service
           _navigationService.GoBackAsync();
        }


        async void NextPage()
        {
            var navParameters = new NavigationParameters();
            navParameters.Add("ProductId", _productId);

            await _navigationService.NavigateAsync(new Uri("AddProductDetailsPage", UriKind.Relative), navParameters);
        } 

        async Task<ObservableCollection<Tag>> ListTagsByCategory()
        {
            return await _networkService.GetApiService()
                    .ListTagsByCategory(token, 25);
        }
        /*
        async Task<ObservableCollection<Tag>> ListTagsByCategory()
        {

            try
            {
                var categories = await _networkService.GetApiService()
                    .ListTagsByCategory(token, 25);

                Items = categories;


            }
            catch (Exception ex)
            {
                //log
                Console.WriteLine(ex);

            }
            finally
            {
                //hide placeholder message

            }

            return null;
        }
        */
        /*
        async Task<ObservableCollection<Category>> ListMainCategories()
        {

            try
            {
                var categories = await _networkService.GetApiService()
                    .ListAllCategories(token);

                return categories;
            }
            catch (Exception ex)
            {
                //log
                Console.WriteLine(ex);

            }
            finally
            {
                //hide placeholder message

            }

            return new ObservableCollection<Category>();
        }
        */


        async Task ListAdvisors()
        {
            try
            {
                var response = await _networkService.GetApiService()
                                                      .ListAdvisor(token, 1);
                //return response.step1;
                StepText = response.step4;
                IsRunning = false;
            }catch
            {
                
            }finally
            {
                IsRunning = false;
            }
        } 


    }
}
