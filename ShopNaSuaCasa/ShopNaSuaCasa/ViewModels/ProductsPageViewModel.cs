﻿using System;
using Prism.Navigation;
using Prism.Commands;

namespace ShopNaSuaCasa.ViewModels
{
    public class ProductsPageViewModel : ViewModelBase
    {
        private INavigationService _navigationService;
        public DelegateCommand NewProductPageCommand { get; set; }

        public ProductsPageViewModel(INavigationService navigationService)
            :base(navigationService)
        {
            _navigationService = navigationService;

            NewProductPageCommand = new DelegateCommand(NewProduct);
        }

        private void NewProduct()
        {
            _navigationService.NavigateAsync(new Uri("NewProductPage", UriKind.Relative));
        }

    
    }
}
