﻿using System;
using Prism.Commands;
using Prism.Navigation;
using System.Collections.Generic;
using ShopNaSuaCasa.Models;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace ShopNaSuaCasa.ViewModels
{
    public class ProductDetailsListPageViewModel : ViewModelBase
    {
        private INavigationService _navigationService;
        public DelegateCommand PreviousPageCommand { get; set; }
        public DelegateCommand NextPageCommand { get; set; }

        public ObservableCollection<Datasheets> _datasheets;
        public ObservableCollection<Datasheets> ListDatasheet
        {
            get { return _datasheets; }
            set { SetProperty(ref _datasheets, value); }
        }
                

        public ProductDetailsListPageViewModel(INavigationService navigationService) 
        : base (navigationService)
        {
            _navigationService = navigationService;
        }

        private void PreviousPage()
        {
            //TODO: call service
            _navigationService.GoBackAsync();
        }

        private void NextPage()
        {
            //TODO: call service
            _navigationService.NavigateAsync(new Uri("PublishProductPage", UriKind.Relative));
        }

    }

   
}
