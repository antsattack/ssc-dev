﻿using System;
using Prism.Navigation;

namespace ShopNaSuaCasa.ViewModels
{
    public class LoginPageViewModel : ViewModelBase
    {
        private INavigationService _navigationService;

        public LoginPageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            _navigationService = navigationService;

        }
    }
}
