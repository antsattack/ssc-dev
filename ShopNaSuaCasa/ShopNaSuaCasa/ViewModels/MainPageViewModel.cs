﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using System.Diagnostics;
using ShopNaSuaCasa.Views;

namespace ShopNaSuaCasa.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {

        #region Properties
        public string Username { get; set; } = "Default Name";

        public string Avatar { get; set; } = "avatar.png";

        public bool IsUserLogout { get; set; } = false;

        public List<OptionsMenu> OptionsMenu { get; } = new List<OptionsMenu>();
        #endregion

        private INavigationService _navigationService;

        public MainPageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            _navigationService = navigationService;

           
           
            OptionsMenu.Add(new OptionsMenu()
            {
                Title = "Produtos",
                Image = "home.png",
                //TargetType = typeof(Pages.Product.ProductsPage)
            });

            OptionsMenu.Add(new OptionsMenu()
            {
                Title = "Pesquisar",
                Image = "home.png",
                //TargetType = typeof(Pages.Product.ProductsPage)
            });

            OptionsMenu.Add(new OptionsMenu()
            {
                Title = "Carrinho de Compras",
                Image = "home.png",
                //TargetType = typeof(Pages.Product.ProductsPage)
            });

            OptionsMenu.Add(new OptionsMenu()
            {
                Title = "Historinco de transacoes",
                Image = "home.png",
                //TargetType = typeof(Pages.Product.ProductsPage)
            });

            OptionsMenu.Add(new OptionsMenu()
            {
                Title = "Plano contratado",
                Image = "home.png",
                //TargetType = typeof(Pages.Product.ProductsPage)
            });

            OptionsMenu.Add(new OptionsMenu()
            {
                Title = "Termos de uso",
                Image = "home.png",
                //TargetType = typeof(Pages.Product.ProductsPage)
            });

            OptionsMenu.Add(new OptionsMenu()
            {
                Title = "Perguntas frequentes",
                Image = "home.png",
                //TargetType = typeof(Pages.Product.ProductsPage)
            });

            OptionsMenu.Add(new OptionsMenu()
            {
                Title = "Suporte",
                Image = "home.png",
                //TargetType = typeof(Pages.Product.ProductsPage)
            });
       
        }


    }
}
