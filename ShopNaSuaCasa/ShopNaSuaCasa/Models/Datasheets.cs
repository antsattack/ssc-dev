﻿using System;
using System.Collections.Generic;
using Prism.Mvvm;
using ShopNaSuaCasa.Interfaces;

namespace ShopNaSuaCasa.Models
{

    public class Datasheets : BindableBase
    {
        public enum Type { Combobox, Color, Switch };

        int id;
        public int Id
        {
            get { return id; }
            set { SetProperty(ref id, value); }
        }

        string name;
        public string Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }

        public Datasheets(int _id, string _name, IList<Object> list)
        {
            this.id = _id;
            this.name = _name;
            this.values = list;
        }

        public IList<Object> values;
        public IList<Object> Values
        {
            get { return values; }
            set { SetProperty(ref value, value); }
        }

        public Type DatasheetType()
        {

            if (values.Contains("sim")) return Type.Switch;

            if (values[0] is Models.Color) return Type.Color;
  
            return Type.Combobox;

        }


    }


}