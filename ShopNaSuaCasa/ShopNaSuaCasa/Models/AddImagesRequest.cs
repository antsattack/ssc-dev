﻿using System;
using System.Collections.Generic;
using System.IO;
using Refit;

namespace ShopNaSuaCasa.Models
{
    public class AddImagesRequest
    {
        public int product { get; set; }
        //public IList<byte[]> file { get; set; }
        public FileInfo[] file { get; set; }
    
    }
}
