﻿using System;
using System.Collections.Generic;
using ShopNaSuaCasa.Models;

namespace ShopNaSuaCasa.Models
{
    public class CategoriesResponse
    {
        public List<Category> Categories { get; set; }
        /*
        public List<Category> AllCategories 
        { 
            get
            {
                List<Category> list = new List<Category>(Categories);
                return list;
            }
           
        }
        */
    }
}
