﻿using System;
using Prism.Mvvm;
namespace ShopNaSuaCasa.Models
{
    public class StepModel : BindableBase
    {

        string _stepText;
        public string StepText
        {
            get { return _stepText; }
            set { SetProperty(ref _stepText, value); }
        }

        bool _isBackButtonEnabled;
        public bool IsBackButtonEnabled
        {
            get { return _isBackButtonEnabled; }
            set { SetProperty(ref _isBackButtonEnabled, value); }
        }

        bool _isNextButtonEnabled;
        public bool IsNextButtonEnabled
        {
            get { return _isNextButtonEnabled; }
            set { SetProperty(ref _isNextButtonEnabled, value); }
        }


        public StepModel(string stepText, bool backButtonEnable, bool nextButtonEnable)
        {
            StepText = stepText;
            IsBackButtonEnabled = backButtonEnable;
            IsNextButtonEnabled = nextButtonEnable;
        }
    }
}
