﻿using System;
using DLToolkit.Forms.Controls;
using Xamarin.Forms;
using ShopNaSuaCasa.Views.Templates;
using System.Collections.Generic;
using ShopNaSuaCasa.ViewModels;

namespace ShopNaSuaCasa.Models
{
    public class TemplateSelectorPageSelector : FlowTemplateSelector
    {
        
        private  DataTemplate SwitchTemplate = new DataTemplate(typeof(TemplateSelectorPageSwitch));
        private DataTemplate ComboboxTemplate = new DataTemplate(typeof(TemplateSelectorPageCombobox));
        private DataTemplate ColorPickerTemplate = new DataTemplate(typeof(TemplateSelectorPageColorPicker));

        protected override DataTemplate OnSelectTemplate(object item, int columnIndex, BindableObject container)
        {
            Console.WriteLine(item);

            Datasheets dataSheets = (Datasheets)item;
            DataTemplate template = null;

            switch (dataSheets.DatasheetType())
            {
                case Datasheets.Type.Color:
                    template = ColorPickerTemplate;
                    break;
                case Datasheets.Type.Switch:
                    template = SwitchTemplate;
                    break;
                default:
                    template = ComboboxTemplate;
                    break;
            } 

            return template;
        }


    }
}
