﻿using System;
using Prism.Mvvm;

namespace ShopNaSuaCasa.Models
{
    public class Field : BindableBase
    {
       
        string _name;
        public String Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        bool _isNotValid;
        public bool IsNotValid
        {
            get { return _isNotValid; }
            set { SetProperty(ref _isNotValid, value); }
        }

        string _notValidMessageError;

        public string NotValidMessageError
        {
            get { return _notValidMessageError; }
            set { SetProperty(ref _notValidMessageError, value); }
        }

        public Field(string name, bool isNotValid, string notValidMessageError)
        {
            _name = name;
            _isNotValid = isNotValid;
            _notValidMessageError = notValidMessageError;

        }

    }

}
