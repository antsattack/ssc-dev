﻿using System;
using System.ComponentModel;
namespace ShopNaSuaCasa.Models
{
    public class NewProductRequest 
    {

        public string title { get; set; }
        public string description { get; set; }

        public NewProductRequest(string _title, string _description)
        {
            title = _title;
            description = _description;

        }

    }
}
