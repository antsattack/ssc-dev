﻿using System;
using Prism.Mvvm;

namespace ShopNaSuaCasa.Models
{
    public class User: BindableBase
    {

        Field _firstName;
        public Field FirstName
        {
            get { return _firstName; }
            set { SetProperty(ref _firstName, value); }
        }

        Field _email;
        public Field Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }

        Field _password;
        public Field Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }
    }
}
