﻿using System;
namespace ShopNaSuaCasa.Models
{
    public class Tag
    {
        public int id { get; set; }
        public string name { get; set; }

        public Tag(int id, string name)
        {
            this.id = id;
            this.name = name;
        }
    }
}
