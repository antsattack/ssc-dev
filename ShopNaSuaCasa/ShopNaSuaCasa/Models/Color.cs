﻿using System;
using Prism.Mvvm;

namespace ShopNaSuaCasa.Models
{
    public class Color : BindableBase
    {

        int _id;
        public int Id
        {
            get { return _id; }
            set { SetProperty(ref _id, value); }
        }

        string _name;
        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        string _hexadecimal;
        public string Hexadecimal
        {
            get { return _hexadecimal; }
            set { SetProperty(ref _hexadecimal, value); }
        }

        public Color(int id, string name, string hexadecimal)
        {
            _id = id;
            _name = name;
            _hexadecimal = hexadecimal;
        }
        /*
        public override string ToString()
        {
            return Name;
        } */
    }
}
