﻿using System;
namespace ShopNaSuaCasa.Models
{
    public class AddImagesResponse
    {
        public string ImageUrl { get; set; }
    }
}
