﻿using System;
namespace ShopNaSuaCasa.Views
{
    public class OptionsMenu
    {
        
        public string Title { get; set; }

        public string Image { get; set; }

        public Type TargetType { get; set; }

    }
}
