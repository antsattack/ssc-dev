﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Diagnostics;
using System.Collections.ObjectModel;

namespace ShopNaSuaCasa.Views
{
    public partial class MainPage : MasterDetailPage
    {
        public MainPage()
        {
            InitializeComponent();

            LeftMenuPage.ListView.ItemSelected += OnMenuItemSelected;
            LeftMenuPage.BackgroundColor = Color.FromHex("#FDAAAB");
            //LeftMenuPage.Padding = new Thickness(0, Device.OnPlatform(20, 0, 0), 0, 0);

            Detail = new NewProductPage();
  
        }

        private void OnMenuItemSelected(object sender, SelectedItemChangedEventArgs e)
        {

            var menuItem = e.SelectedItem as OptionsMenu;
            if (menuItem == null)
                return;
            /*
            var page = (Page)Activator.CreateInstance(menuItem.TargetType);
            page.Title = menuItem.Title;

            Detail = App.RootNavigationPage = new NavigationPage(page)
            {
            };
            IsPresented = false;

            LeftMenuPage.ListView.SelectedItem = null; */
        }

       
    }
}