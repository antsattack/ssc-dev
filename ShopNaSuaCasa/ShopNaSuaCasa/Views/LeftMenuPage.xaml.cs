﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace ShopNaSuaCasa.Views
{
    public partial class LeftMenuPage : ContentPage
    {
        public ListView ListView;

        public LeftMenuPage()
        {
            InitializeComponent();

            //BindingContext = App.Locator.Menu;
            ListView = MenuItemsListView;
            BackgroundColor = Color.FromHex("#DDDDDD");
        }
    }
}
