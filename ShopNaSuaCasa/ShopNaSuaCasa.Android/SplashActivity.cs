﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;

namespace ShopNaSuaCasa.Droid
{
    [Activity(Theme = "@style/MyTheme.Splash",
              NoHistory = true,
              Icon = "@drawable/icon",
              MainLauncher = true,
              ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
              ScreenOrientation = ScreenOrientation.Portrait)]
    public class SplashActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            var intent = new Intent(this, typeof(MainActivity));
            StartActivity(intent);
            Finish();
        }
    }
}
