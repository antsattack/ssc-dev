﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Prism;
using Prism.Ioc;
using CarouselView.FormsPlugin.Android;
using FFImageLoading.Forms.Platform;
using Xfx;

namespace ShopNaSuaCasa.Droid
{
    [Activity(Label = "ShopNaSuaCasa", Icon = "@mipmap/icon", 
              Theme = "@style/MainTheme", 
              ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);
            XfxControls.Init();
            global::Xamarin.Forms.Forms.Init(this, bundle);

            //libs
            CarouselViewRenderer.Init();
            CachedImageRenderer.Init(enableFastRenderer: true);
            Plugin.CurrentActivity.CrossCurrentActivity.Current.Init(this, bundle);
            Plugin.CurrentActivity.CrossCurrentActivity.Current.Activity = this;
            LoadApplication(new App(new AndroidInitializer()));
            Window.SetStatusBarColor(Android.Graphics.Color.Argb(255, 254, 192, 193));
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Android.Content.PM.Permission[] grantResults)
        {
            Plugin.Permissions.PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public class AndroidInitializer : IPlatformInitializer
    {
        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            // Register any platform specific implementations
        }
    }
}

